﻿function Get-SurfaceWater {
  param (
    [Int] $PageSize = 50,
    [Int] $Page = 0
  )

  $Response = Invoke-RestMethod "http://data.sepa.org.uk/water/doc/water/surfacewaters.json?_pageSize=$PageSize&_page=$Page"
  $Response.result.items
}
