﻿$AllItems = @()

foreach ($page in 1..100) {
  $Items = Get-SurfaceWater -Page $page
  if (-not $Items) {
    break
  }

  $AllItems += $Items
}

$AllItems |
Export-Csv -Path C:\Users\iain\Desktop\surfacewater.csv
